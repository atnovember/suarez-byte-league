import React, {useEffect, useMemo, useState} from 'react';
import SerieATable from '../../data/leagues/ItalySerieATables.json';
// import SerieBTable from '../../data/leagues/ItalySerieBTables.json'



const TeamList = ({onClick}) => {
  //  Line 8:9:  The 'loadedDataSerieA' array makes the dependencies of useEffect Hook (at line 37) change on every render. Move it inside the useEffect callback. Alternatively, wrap the initialization of 'loadedDataSerieA' in its own useMemo() Hook  react-hooks/exhaustive-deps
  // const loadedDataSerieA = [...SerieATable];
  const loadedDataSerieA = useMemo(() => [...SerieATable], []);
  // const loadedDataSerieA = useMemo(() => [], []);
  // const loadedDataSerieB = [...SerieBTable];
  const [teams, setTeams] = useState([])

  useEffect(() => {
    const teams2 = [];

    // заменила map на each надеюсь будет работать верно
    loadedDataSerieA.forEach((season) => {
      const key = Object.keys(season)[0];
      season[key].table.forEach((tableRow) => {
          if (!teams2.includes(tableRow.club)) {
            teams2.push(tableRow.club);
          }
        });
    });

    // loadedDataSerieB.map((season) => {
    //   const key = Object.keys(season)[0];
    //   season[key].table.map(tableRow => {
    //       console.log('tableRow', tableRow.club)
    //       if (!teams2.includes(tableRow.club)) {
    //         teams2.push(tableRow.club);
    //       }
    //     }
    //   )
    // });
    setTeams(teams2);
    // todo: убрать правила линта, чтобы не ругался на эти зависимости
  }, [loadedDataSerieA])

  return (
    <div style={{ width: '20%', maxWidth: '200px', float: 'left', background: 'rgba(0,0,0, 0.5)', color: 'white' }}>
      <div style={{ height: '20px', marginTop: '10px', marginBottom: '10px', paddingLeft: '10px' }}>
        Выберите команду:
      </div>
      <ul style={{ height: 'calc(100% - 40px)', overflowY: 'scroll', margin: 0, marginBottom: '10px' }}>
      {
        teams.map(team => <li onClick={() => onClick(team)}>{team}</li>)
      }
      </ul>
    </div>
  )
}
export default TeamList;
