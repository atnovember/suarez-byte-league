import React, {useCallback, useEffect, useMemo, useState} from 'react';
import SerieATable from '../../data/leagues/ItalySerieATables.json';
import SerieBTable from '../../data/leagues/ItalySerieBTables.json';

import {CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from 'recharts';


/*
* график для 1 команды ( в данном случае Juventus) в 1 лиге (потом сделать несколько команд и несколько лиг
* */
const RankGraphic = ({team, league, hideAxisX}) => {

  // При разработке особой рои не играют, а вот при билде...
  // todo: как оно транспилистся??
  //  const loadedDataSerieA = useMemo(() => [...SerieATable]);
  //  const loadedDataSerieA = [...SerieATable];
  const loadedDataSerieA = useMemo(() => [...SerieATable], []);
  const loadedDataSerieB = useMemo(() => [...SerieBTable], []);

  const [ranks, setRanks] = useState([])

  ///Line 16:9:  The 'sourceData' function makes the dependencies of useEffect Hook (at line 56) change on every render. To fix this, wrap the definition of 'sourceData' in its own useCallback() Hook  react-hooks/exhaustive-deps
  // const sourceData = () => {
  //   switch (league) {
  //     case 'Serie A':
  //       return loadedDataSerieA;
  //     case 'Serie B':
  //       return loadedDataSerieB;
  //     default:
  //       return [];
  //   }
  // }

  const sourceData = useCallback(() => {
    switch (league) {
      case 'Serie A':
        return loadedDataSerieA;
      case 'Serie B':
        return loadedDataSerieB;
      default:
        return [];
    }
  }, [league, loadedDataSerieA, loadedDataSerieB])

  console.log('sourceData', sourceData())
  useEffect(() => {
    const ranks = [];


    // заменила map на each надеюсь будет работать верно
    sourceData().forEach((season) => {
      console.log('season', season)
      const key = Object.keys(season)[0];
      season[key].table.forEach(tableRow => {
        console.log('tableRow >>', tableRow)

        // если клуба нет, он может быть в другой лиге, или переименован
        // todo: добавить проверятор из другий лиги
        // todo: добавить проверятор из других имен
        // хотя, год надо добавлять в любом случае, есть там клуб или нет, если нет - добавлять год без значения
          if (tableRow.club === team && season[key].league === league) {
            ranks.push({name: key, rank: tableRow["#"] } );
          } else {
            ranks.push({name: key } );
          }
        }
      )
    });

    setRanks(ranks);
    console.log('ranks>>> ', ranks )
    // todo: убрать правила линта, чтобы не ругался на эти зависимости
  }, [league, sourceData, team])



  return (
    <ResponsiveContainer width="100%" height={300}>
      <LineChart
        width={500}
        height={400}
        data={ranks}
        margin={{
          top: 10,
          right: 30,
          left: 0,
          bottom: 0,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" hide={hideAxisX}/>
        <XAxis dataKey="name" />
        {/*<XAxis dataKey="name" />*/}
        <YAxis domain={[1, 20]} label={{ value: league, angle: -90, position: 'insideLeft', fill: 'white' }} reversed/>
        <Tooltip />
        <Line type="monotone" dataKey="rank" stroke="#fff" fill="#fff" />
      </LineChart>
    </ResponsiveContainer>
  )
}
export default RankGraphic;
