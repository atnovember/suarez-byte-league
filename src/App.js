import React, { useState} from 'react';
import TeamList from './components/TeamList';
import RankGraphic from './components/RankGraphic';
import background from "./assets/bg.jpg";
//
// const data = [
//   { name: '1929', uv: 2 },
//   { name: '1930', uv: 10 },
//   { name: 'Page C', uv: 20 },
//   { name: 'Page D' },
//   { name: 'Page E', uv: 20},
//   { name: 'Page F', uv: 10 },
//   { name: 'Page G', uv: 2 },
//   { name: '1929', uv: 2 },
//   { name: '1930', uv: 10 },
//   { name: 'Page C', uv: 20 },
//   { name: 'Page D' },
//   { name: 'Page E', uv: 20},
//   { name: 'Page F', uv: 10 },
//   { name: 'Page G', uv: 2 },
// ];
//
// const data2 = [
//   { name: '1929' },
//   { name: '1930' },
//   { name: '1931', uv: 3 },
//   { name: '1932', uv: 10 },
//   { name: '1933', uv: 1},
//   { name: '1934' },
//   { name: '1929' },
//   { name: '1929' },
//   { name: '1930' },
//   { name: '1931', uv: 3 },
//   { name: '1932', uv: 10 },
//   { name: '1933', uv: 1},
//   { name: '1934' },
//   { name: '1929' },
// ];

const LeagueHistory = () => {
  // const demoUrl = 'https://codesandbox.io/s/line-chart-connect-nulls-sqp96';
  const [team, setTeam] = useState('');

  const checkTeam = (team) => {
    console.log('onClick', team);
    setTeam(team);
  }

    return (
      <div style={{
        display: 'flex',
        // background: 'pink',
        backgroundImage: `url(${background})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        height: '100vh',
        width: '100%',
        opacity: 0.75
      }}>
        <TeamList onClick={checkTeam} />
        <div style={{ width: '100%', float: 'right'}}>


          <div style={{color: '#fff', paddingLeft: '10px', background: 'rgba(0,0,0, 0.75)'}}>
            <h3 style={{paddingLeft: '60px'}}> История перемещения команды {team} в лиге за всю историю сущетсвования лиги: </h3>

            <RankGraphic
              team={team}
              league={'Serie A'}
              hideAxisX={true}
            ></RankGraphic>

            <RankGraphic
              team={team}
              league={'Serie B'}
            ></RankGraphic>
          </div>



          {/*<ResponsiveContainer width="100%" height={200}>*/}
          {/*  <LineChart*/}
          {/*    width={500}*/}
          {/*    height={200}*/}
          {/*    data={data2}*/}
          {/*    margin={{*/}
          {/*      top: 10,*/}
          {/*      right: 30,*/}
          {/*      left: 0,*/}
          {/*      bottom: 0,*/}
          {/*    }}*/}
          {/*  >*/}
          {/*    <CartesianGrid strokeDasharray="3 3" />*/}
          {/*    <XAxis dataKey="name" />*/}
          {/*    <YAxis domain={[0,30]} label={{ value: "Serie B", angle: -90, position: 'insideLeft' }} reversed/>*/}
          {/*    <Tooltip />*/}
          {/*    <Line type="monotone" dataKey="uv" stroke="#8884d8" fill="#8884d8" />*/}
          {/*  </LineChart>*/}
          {/*</ResponsiveContainer>*/}
        </div>
      </div>
    );
}

export default LeagueHistory;
