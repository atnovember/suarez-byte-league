const laLiga = require('./laLiga');

function generateIdTeamList(data) {
  /**
   * transfermarktId
   * title
   * logoUrl
   * */
  const clubs = []
  for (let season in data) {
    data[season].map((club) => {

      if (clubs.some(duplicatedClub => club.id === duplicatedClub.transfermarktId)) {
        console.log('duplicate', club.title)
      } else {
        const newClub = {
          transfermarktId: club.id,
          title: club.title,
          logoUrl: club.logoUrl,
        }

        clubs.push(newClub);
      }
    })
  }

  console.log('clubs', clubs)
}

generateIdTeamList(laLiga);